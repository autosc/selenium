package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriver_VerifyTitle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
        String baseUrl = "http://www.mercurytours.com";
        String expectedTitle = "Welcome: Mercury Tours";
        String actualTitle = "";
		
		System.setProperty("webdriver.chrome.driver", "D://Ritu//Testing Training//Selenium//chromedriver_win32//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get(baseUrl);
		actualTitle = driver.getTitle();
		
		if (actualTitle.contentEquals(expectedTitle)){
			System.out.println ("Titles match");
		}
		else {System.out.println ("Titles do not match");}
		}
			

}


