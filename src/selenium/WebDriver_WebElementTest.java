package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriver_WebElementTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String expectedTitle = "Facebook";
		String actualTitle = "";

		// Setting the system properties of Chrome driver
		System.setProperty("webdriver.chrome.driver",
				"D://Ritu//Testing Training//Selenium//chromedriver_win32//chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.get("http://www.facebook.com");
		driver.manage().window().maximize();

		actualTitle = driver.getTitle();
		if (actualTitle.equals(expectedTitle)) {
			System.out.println("Titles match");
		} else {
			System.out.println("Titles do not match");
			{
			}

			WebElement email = driver.findElement(By.id("email"));
			email.sendKeys("ABC");

			WebElement password = driver.findElement(By.name("pass"));
			password.sendKeys("XYZ");

			WebElement login = driver.findElement(By.name("login"));
			login.submit();

			driver.close();

		}
	}
}
